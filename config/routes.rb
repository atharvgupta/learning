Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
   root 'customers#index'
   get 'signup', to: 'customers#new'
   post 'signup', to: 'customers#create'
   get 'login', to: 'customers#login'
   post 'login', to: 'customers#check_user_detail'
   post 'book', to: 'customers#book_rooms'
   resources :customers
end
