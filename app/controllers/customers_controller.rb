class CustomersController < ApplicationController
	def login
		@customer=Customer.new
	end

	def new
    	@customer = Customer.new
	end

	def create
	    @customer = Customer.new(params.require(:customer).permit(:name,:contact,:gender,:email,:password))
	    if @customer.save
   			redirect_to login_path
   		else
   			render json: {status: 401}
   		end
	end

	def list_user(user)
		render json: {
  			status: :created,
  			logged_in: true,
  			user: user
  		}
	end

	def check_user_detail
		user = Customer.find_by_email(params["customer"]["email"])
        if user && user.authenticate(params["customer"]["password"])
        	puts "in if block "
        	session[:user_id] = user.id
        	@customer=user
        	available_rooms
        	p @normal_room_count
            render "login_success"
        else
        	p "inside"
            render "login_failure"
        end
    end

    def book_rooms
    	available_rooms
		normal_rooms=params["booking"]["normal_rooms"]
		luxury_rooms=params["booking"]["luxury_rooms"]
		if((@normal_room_count.to_i - normal_rooms.to_i)<0 || (@luxury_room_count.to_i - luxury_rooms.to_i)<0)
			render "login_failure"
		else
			render json: {status: 'success'}
		end
	end


	private 

	def available_rooms
		@total_room_count=Room.where("occupied=0").count('room_no')
		@normal_room_count=Room.where("occupied=0 and room_type_id=10").count('room_no')
		@luxury_room_count=Room.where("occupied=0 and room_type_id=11").count('room_no') 
	end

end