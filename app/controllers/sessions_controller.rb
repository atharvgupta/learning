class SessionsController < ApplicationController
    def new
    end
    def index
    end
    def create
        user = Customer.find_by_email(params[:email])
        if user && user.authenticate(params[:password])
            session[:user_id] = user.id
            render "index"
         else
            render "new"
        end
        end

    def destroy
        session[:user_id] = nil
        redirect_to root_url, notice: "Logged out!"
    end
end