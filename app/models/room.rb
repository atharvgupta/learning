class Room < ApplicationRecord
	has_many :bookings,foreign_key: :room_no
	belongs_to :room_type
end