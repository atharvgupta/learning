class Customer < ApplicationRecord
	has_secure_password
	# attr_accessible :email, :password
    validates_uniqueness_of :email
	has_many :bookings
end