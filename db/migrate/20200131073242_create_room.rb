class CreateRoom < ActiveRecord::Migration[5.0]
  def change
    create_table :rooms, id:false do |t|
      t.integer :room_no,primary_key: true, auto_increment:true
      t.integer :room_type_id
      t.integer :floor
      t.boolean :occupied
    end
  end
end
