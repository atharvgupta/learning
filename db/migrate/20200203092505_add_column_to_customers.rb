class AddColumnToCustomers < ActiveRecord::Migration[5.0]
  def change
  	add_column :customers, :email, :string, unique: true, null: false  
	add_column :customers, :password_digest, :string, null: false  
  end
end
