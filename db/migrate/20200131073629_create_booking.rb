class CreateBooking < ActiveRecord::Migration[5.0]
  def change
    create_table :bookings,id:false do |t|
      t.integer :booking_id,primary_key: true, auto_increment:true
      t.integer :customer_id
      t.integer :room_no
      t.date :check_in
      t.date :check_out
      t.integer :total_amount
    end
  end
end
