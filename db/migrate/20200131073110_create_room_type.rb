class CreateRoomType < ActiveRecord::Migration[5.0]
  def change
    create_table :room_types, id:false do |t|
      t.integer :room_type_id,primary_key: true, auto_increment:true
      t.string :room_type
      t.integer :rate_per_day
    end
  end
end
