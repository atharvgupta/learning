class CreateCustomers < ActiveRecord::Migration[5.0]
  def change
    create_table :customers, id: false do |t|
      t.integer :customer_id, primary_key: true, auto_increment:true
      t.string :name
      t.string :contact
      t.string :gender
    end
  end
end
